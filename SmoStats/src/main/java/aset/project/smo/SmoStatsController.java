package aset.project.smo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SmoStatsController {

    @Autowired
    private SmoStatsRepository smoStatsRepository;

    @RequestMapping(value = "/smo-stats", method = RequestMethod.POST)
    public void addStats(@RequestBody SmoStats smoStats) {
        System.out.println("smo-stats - post");
        smoStatsRepository.save(smoStats);
    }

    @RequestMapping(value = "/smo-stats", method = RequestMethod.GET)
    public Iterable<SmoStats> getStats() {
        System.out.println("smo-stats - get");
        return smoStatsRepository.findAll();
    }

}
