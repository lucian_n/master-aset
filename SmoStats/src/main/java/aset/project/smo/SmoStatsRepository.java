package aset.project.smo;

import aset.project.smo.SmoStats;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:nevol@amazon.com">Lucian Nevoe</a>
 */
@Repository
public interface SmoStatsRepository extends CrudRepository<SmoStats, Integer> {
}
