package aset.project.smo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

/**
 * @author <a href="mailto:nevol@amazon.com">Lucian Nevoe</a>
 */
@Entity
public class SmoStats {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    private String w;
    private double b;
    private int trainInstancesNumber;
    private int testInstancesNumber;
    private int trainingTime;
    private double accuracy;
    private String kernelId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public int getTrainInstancesNumber() {
        return trainInstancesNumber;
    }

    public void setTrainInstancesNumber(int trainInstancesNumber) {
        this.trainInstancesNumber = trainInstancesNumber;
    }

    public int getTestInstancesNumber() {
        return testInstancesNumber;
    }

    public void setTestInstancesNumber(int testInstancesNumber) {
        this.testInstancesNumber = testInstancesNumber;
    }

    public int getTrainingTime() {
        return trainingTime;
    }

    public void setTrainingTime(int trainingTime) {
        this.trainingTime = trainingTime;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public String getKernelId() {
        return kernelId;
    }

    public void setKernelId(String kernelId) {
        this.kernelId = kernelId;
    }
}
