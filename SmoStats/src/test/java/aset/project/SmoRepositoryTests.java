package aset.project;

import aset.project.smo.SmoStats;
import aset.project.smo.SmoStatsRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmoRepositoryTests {

	@Autowired
	private SmoStatsRepository smoStatsRepository;

	private static SmoStats smoStats;

	static {
		smoStats = new SmoStats();
		smoStats.setId(1);
		smoStats.setAccuracy(90.5);
		smoStats.setTrainingTime(1234);
	}


	@Before
	public void before() {
		smoStatsRepository.save(smoStats);
	}

	@Test
	@Transactional
	public void testDelete() {
		smoStatsRepository.delete(1);
		Assert.assertEquals(0, size(smoStatsRepository.findAll()));
	}

	@Test
	@Transactional
	public void testAdd() {
		//given
		SmoStats smoStats = new SmoStats();
		smoStats.setId(2);
		smoStats.setAccuracy(91.5);
		smoStats.setTrainingTime(1234);

		//when
		smoStatsRepository.save(smoStats);

		//then
		Assert.assertEquals(2, size(smoStatsRepository.findAll()));
	}

	private int size(Iterable<SmoStats> all) {
		int n = 0;
		for (SmoStats smoStats : all) {
			n++;
		}
		return n;
	}



}
